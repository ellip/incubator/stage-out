import os
import sys
import json
from pystac import Catalog, Collection, EOItem, MediaType, EOAsset, CatalogType
from shapely.geometry import shape
import datetime
from .atom import Atom
from .ops import post_atom
import hashlib
import requests
import click
import logging
#from webdav3.client import Client
import owncloud

logging.basicConfig(stream=sys.stderr, 
                    level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%S')

template = """<?xml version="1.0"?>
    <feed xmlns="http://www.w3.org/2005/Atom">
    <entry>
        <title type="text"></title>
        <summary type="html"></summary>
        <link rel="enclosure" type="application/octet-stream" href=""/>
        <date xmlns="http://purl.org/dc/elements/1.1/"></date>
        <published></published>
        <identifier xmlns="http://purl.org/dc/elements/1.1/"></identifier>
    </entry>
    </feed>"""

def make_dir(client, path):
    
    try:
        client.file_info(path).is_dir()
    except owncloud.ResponseError as e:
        if e.status_code == 404:
            client.mkdir(path)
    

@click.command()
@click.option('--job', 'job',  required=True, help='job identifer')
@click.option('--source', 'source', required=True, help='results JSON file')
@click.option('--endpoint', 'endpoint', required=True, help='catalog endpoint')
@click.option('--username', 'username', required=True, help='catalog username')
@click.option('--apikey', 'api_key', required=True, help='catalog api key')
@click.option('--store-host', 'store_host', required=True, help='store endpoint')
@click.option('--store-username', 'store_username', required=True, help='store username')
@click.option('--store-apikey', 'store_api_key', required=True, help='store api key')
@click.option('--outputfile', 'outputfile', required=True, help='outputfile')
def main(job, source, endpoint, username, api_key, store_host, store_username, store_api_key,outputfile):

    oc = owncloud.Client(store_host)
    
    oc.login(store_username, 
             store_api_key)
    
    with open(source) as json_file:
        results = json.load(json_file)

    if  isinstance(results['wf_outputs'][0],list):
        base_path = results['wf_outputs'][0][0]['path']
        listing = results['wf_outputs'][0][0]['listing']
    else:
        base_path = results['wf_outputs'][0]['path']
        listing = results['wf_outputs'][0]['listing']
     
    logging.info(base_path)
    
    os.chdir(base_path)
    
    for index, listing in enumerate(listing):

        if listing['class'] == 'Directory':

            if listing['class'] == 'Directory':

                for sublisting in listing['listing']:
                    if (sublisting['basename']) == 'catalog.json':
                        stac_catalog = sublisting['location']

                        break

    cat = Catalog.from_file(stac_catalog.replace('file://', ''))

    uids = []
    
    for item in cat.get_items():

        logging.info(item.id)

        for index, asset in item.assets.items():
            
            remote_file = '/'.join([job, asset.get_absolute_href().replace(base_path + '/', '')])
            local_file = asset.get_absolute_href().replace(base_path, '.')
  
            # create folders one after the other
            for index, p in enumerate(os.path.dirname(remote_file).split('/')):
                
                make_dir(oc, 
                         '/'.join(os.path.dirname(remote_file).split('/')[0:index+1]))
            

            # upload asset
            logging.info('Publishing {} to {}'.format(local_file, remote_file))
            
            oc.put_file(remote_file, 
                        local_file)

            # this is the link
            link_info = oc.share_file_with_link(remote_file)

            logging.info('Here is the link to the asset: {}'.format(link_info.get_link()))
            
            # create the atom
            atom = Atom.from_template(template)
            
            atom.set_title('{} {}'.format(item.id, asset.title))
            atom.set_dcdate(item.datetime.strftime('%Y-%m-%dT%H:%M:%SZ'))
            atom.set_published('{0}Z'.format(datetime.datetime.now().isoformat()))
            
            # create a nice identifier
            m = hashlib.md5()
            m.update('{} {}'.format(item.id, asset.title).encode('utf-8'))

            uids.append({'{geo:uid}': m.hexdigest()})
            
            atom.set_identifier(m.hexdigest())
            atom.set_dctspatial(shape(item.geometry).wkt)
            atom.set_summary('{} {} ({})'.format(item.id, asset.title, asset.media_type))

            enclosure = '{}/remote.php/dav/files/{}/{}'.format(store_host if store_host[-1] != '/' else store_host[:-1],
                                                               store_username,
                                                               remote_file)
            
            atom.set_enclosure_link(href=enclosure,
                                    title=asset.title,
                                    mime_type=asset.media_type)

            # post the atom
            r = post_atom(atom,
                          endpoint,
                          username, 
                          api_key)
        
            logging.info(r.status_code)
            logging.info(r.text)
            
        # publish the item.json
        for link in item.get_links():

            if link.rel == 'self':

                remote_file = '/'.join([job, link.target.replace(base_path + '/', '')])
                local_file = link.target.replace(base_path, '.')
    
                oc.put_file(remote_file, 
                            local_file)

    # publish the catalog.json file
    remote_file = '/'.join([job, stac_catalog.replace('file://', '').replace(base_path + '/', '')])
    local_file = stac_catalog.replace('file://', '').replace(base_path, '.')
    
    oc.put_file(remote_file, 
                local_file)
    
    enclosure = '{}/remote.php/dav/files/{}/{}'.format(store_host if store_host[-1] != '/' else store_host[:-1],
                                                       store_username,
                                                       remote_file)
    
    output = {'stac:catalog': {'href': enclosure}, 
              'opensearch': {'href': endpoint, 'query':uids}}
    
    print(output)

    if outputfile != None:
        with open(outputfile, 'w') as outfile:
            json.dump(output, outfile)

            
if __name__ == "__main__":
    main()





